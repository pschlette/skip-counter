import express from "express";
import compression from "compression";  // compresses requests
import session from "express-session";
import bodyParser from "body-parser";
import logger from "morgan";
import lusca from "lusca";
import dotenv from "dotenv";
import mongo from "connect-mongo";
import flash from "express-flash";
import path from "path";
import mongoose from "mongoose";
import passport from "passport";
import PassportGoogleTokenStrategy from "passport-google-id-token";
import expressValidator from "express-validator";
import bluebird from "bluebird";

const GOOGLE_ID = process.env.GOOGLE_ID;
const GOOGLE_SECRET = process.env.GOOGLE_SECRET;

console.log(`Google client id is ${GOOGLE_ID}`);

const MongoStore = mongo(session);

// Controllers (route handlers)
import * as homeController from "./controllers/home";

// Passport config
passport.use(new PassportGoogleTokenStrategy({
  clientId: GOOGLE_ID
}, (parsedToken: any, googleId: string, done: Function) => {
  console.log(JSON.stringify(parsedToken, undefined, 2));
  return done(undefined, googleId);
}));

passport.serializeUser((user, done) => {
  done(undefined, user);
});

passport.deserializeUser((userId, done) => {
  // req.user will just be the user's id, for now
  done(undefined, userId);
});

// Create Express server
const app = express();

// Connect to MongoDB
const mongoUrl = process.env.MONGOLAB_URI;
(<any>mongoose).Promise = bluebird;
mongoose.connect(mongoUrl, {useMongoClient: true}).then(
  () => { /** ready to use. The `mongoose.connect()` promise resolves to undefined. */ },
).catch(err => {
  console.log("MongoDB connection error. Please make sure MongoDB is running. " + err);
  // process.exit();
});

// Express configuration
app.set("port", process.env.PORT || 3000);
app.set("views", path.join(__dirname, "../views"));
app.set("view engine", "pug");
app.use(compression());
app.use(logger("dev"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(expressValidator());
app.use(session({
  resave: true,
  saveUninitialized: true,
  secret: process.env.SESSION_SECRET,
  store: new MongoStore({
    url: mongoUrl,
    autoReconnect: true
  })
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());
app.use(lusca.xframe("SAMEORIGIN"));
app.use(lusca.xssProtection(true));
app.use((req, res, next) => {
  res.locals.user = req.user;
  next();
});
app.use((req, res, next) => {
  // After successful login, redirect back to the intended page
  if (!req.user &&
    req.path !== "/login" &&
    req.path !== "/signup" &&
    !req.path.match(/^\/auth/) &&
    !req.path.match(/\./)) {
    req.session.returnTo = req.path;
  } else if (req.user &&
    req.path == "/account") {
    req.session.returnTo = req.path;
  }
  next();
});

app.use(
  express.static(path.join(__dirname, "public"), { maxAge: 31557600000 })
);

app.get("/somejson", (req, res) => {
  res.json({
    aKey: "aValue"
  });
});

app.post("/google_signin",
  passport.authenticate("google-id-token"),
  (req, res) => {
    console.log("do we have the user?");
    console.log(JSON.stringify(req.user, undefined, 2));
    res.json(req.user);
  }
);

/**
 * Primary app routes.
 */
app.get("/", homeController.index);

export default app;

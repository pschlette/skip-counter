/// <reference types="gapi.auth2" />

import * as React from 'react';
import * as style from './style.css';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import { RouteComponentProps } from 'react-router';
import { RootState } from 'app/reducers';
import { TodoModel } from 'app/models';

export namespace App {
  export interface Props extends RouteComponentProps<void> {
    todos: RootState.TodoState;
    actions: {};
    filter: TodoModel.Filter;
  }
}

@connect(
  // (state: RootState): Pick<App.Props, 'todos' | 'filter'> => {
  (state: RootState): {} => ({ }),
  (dispatch: Dispatch<RootState>): Pick<App.Props, 'actions'> => ({
    actions: bindActionCreators({}, dispatch)
  })
)
export class App extends React.Component<App.Props> {
  static defaultProps: Partial<App.Props> = {
  };

  constructor(props: App.Props, context?: any) {
    super(props, context);
  }

  componentDidMount() {
    (window as any).onSignIn = (googleUser: gapi.auth2.GoogleUser) => {
      console.log(googleUser.getAuthResponse().id_token);
    }
  }

  render() {
    return (
      <div>
        <div className="g-signin2" data-onsuccess="onSignIn" data-theme="dark"></div>
        <div className={style.normal}>
          Hello, world.
        </div>
      </div>
    );
  }
}

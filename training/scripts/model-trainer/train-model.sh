# consider window_size_ms and window_stride_ms ?
python \
  tensorflow/examples/speech_commands/train.py \
  --data_url= \
  --data_dir=./classified-clips \
  --clip_duration_ms=300 \
  --wanted_words=thwack \
  --unknown_percentage=20 \
  --time_shift_ms=30 \
  --model_architecture=low_latency_conv \
  --how_many_training_steps=200,60 \
  --learning_rate=0.01,0.001

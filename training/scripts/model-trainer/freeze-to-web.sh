tensorflowjs_converter \
  --input_format=tf_session_bundle \
  --output_node_names='thwack,not-a-thwack' \
  'tensorflow-output/speech_commands_train' \
  'tensorflow-output/web_model'

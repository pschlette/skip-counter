python \
  tensorflow/examples/speech_commands/freeze.py \
  --wanted_words=thwack \
  --model_architecture=low_latency_conv \
  --clip_duration_ms=300 \
  --start_checkpoint=/tmp/speech_commands_train/low_latency_conv.ckpt-260 \
  --output_file=/tmp/frozen_skip_counter_graph.pb

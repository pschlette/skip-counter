import glob
import json
import math
import os
import shutil
from pydub import AudioSegment

TIMESTAMPS_JSON_PATH = 'timestamps.json'

# should only contain raw recordings in WAV format (and no sub-directories)
FULL_WAVS_DIR = 'raw-training-recordings'

OUTPUT_CHUNKS_DIR = 'classified-clips'

# TODO - control this with an env var? or something
CHUNK_LENGTH_MS = 300

# clean output dir
if os.path.exists(OUTPUT_CHUNKS_DIR):
    shutil.rmtree(OUTPUT_CHUNKS_DIR)
os.mkdir(OUTPUT_CHUNKS_DIR)

with open(TIMESTAMPS_JSON_PATH) as f:
    unparsed_timestamps_json = f.read()

timestamps_json = json.loads(unparsed_timestamps_json)

# for each track that we have a list of timestamps for
for base_name in timestamps_json.keys():
    full_path = f"{FULL_WAVS_DIR}/{base_name}.wav"
    full_track = AudioSegment.from_wav(full_path)
    full_track_length_seconds = full_track.duration_seconds

    for chunk_type in timestamps_json[base_name]:
        chunk_dir_path = f"{OUTPUT_CHUNKS_DIR}/{chunk_type}"
        if not os.path.exists(chunk_dir_path):
          os.mkdir(chunk_dir_path)
        timestamp_list = timestamps_json[base_name][chunk_type]

        # timestamps are provided as a float, in seconds
        for thwack_start_timestamp_seconds in timestamp_list:
            timestamp_ms = int(thwack_start_timestamp_seconds * 1000)
            chunk_start_ms = timestamp_ms
            chunk_end_ms = chunk_start_ms + CHUNK_LENGTH_MS
            chunk = full_track[chunk_start_ms:chunk_end_ms]

            padding_zeroes_count = math.floor(math.log10(full_track_length_seconds * 1000)) + 1
            formatted_timestamp_ms = str(timestamp_ms).zfill(padding_zeroes_count)

            chunk.export(
                f"{OUTPUT_CHUNKS_DIR}/{chunk_type}/{base_name}_{formatted_timestamp_ms}.wav",
                format='wav'
            )
